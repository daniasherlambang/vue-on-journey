import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import axios from "axios";
import http from "http";
import TokenStorageService from '../storage/TokenStorageService'

Vue.use(VueRouter);

// // configure router
// const router = new VueRouter({
//   routes, // short for routes: routes
//   linkActiveClass: 'active',
//   scrollBehavior: (to, from ,savedPosition) => {
//     if (savedPosition) {
//       return savedPosition;
//     }
//     if (to.hash) {
//       return { selector: to.hash };
//     }
//     return { x: 0, y: 0 };
//   }
// });

const router = new VueRouter({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes, // short for routes: routes
})

// LocalstorageService
const localStorageService = TokenStorageService.getService();


router.beforeEach((to, from, next) => {
  // console.log('assalamualaikum: '+ localStorageService.getAccessToken())
  // console.log('you are navigating from: '+ from.name)
  // console.log('you are navigating to: '+ to.name)
  if ( to.name !== 'Login' && to.name !== 'Register' && to.name !== 'Home'){
      if(localStorageService.getAccessToken() == null || localStorageService.getRefreshToken() == null){
        // console.log('movetologin')
        console.log(localStorageService.getAccessToken())
        router.push('/login')
      }else{
        axios.get('/user/checker_token').then(res =>{
          if(res.status===200){
            // console.log('next 1')
            next()
          }

        })

      }
    }
  else next()

})





// Add a request interceptor
axios.interceptors.request.use(
   config => {
       const token = localStorageService.getAccessToken();
       const refresh = localStorageService.getRefreshToken();
       console.log('req url: '+config.url)
       console.log('access token: '+ token)
      //  ini kode jancukin. nggarai mumet. redirect should be handled on response, not request
      //  console.log((config.url !== '/user/login'))
      //  if((config.url !== '/user/login/refresh'&&config.url !== '/user/login')&&
      //  (token==null||refresh==null)){
      //   console.log('should go')
      //   router.push('/Login')
      //   return
      //  }
       console.log('bar iki cek token')
       if (token) {
           config.headers['Authorization'] = 'Bearer ' + token;
       }
       console.log('set header')
       config.headers['Content-Type'] = 'application/json';
       console.log(config)

       return config;
   },
   error => {
       Promise.reject(error)
   });



//Add a response interceptor

axios.interceptors.response.use((response) => {
   return response
}, function (error) {
   const originalRequest = error.config;
   console.log( error)
   if (error.response.status === 401 && originalRequest.url === '/user/login/refresh') {

    //  console.log('should go to login')
        localStorageService.clearToken();
       router.push('/Login');
       return Promise.reject(error);
   }

   if (error.response.status === 401 && !originalRequest._retry) {

       originalRequest._retry = true;
      //  console.log('do refresh')
       const refreshToken = localStorageService.getRefreshToken();
      //  console.log('refresh token: '+refreshToken)
       return axios.post('/user/login/refresh',
       {
         "refresh": refreshToken
       }).then(
         res =>{
           if (res.status === 200) {
            // console.log(res.data);
            localStorageService.setAccessToken(res.data.access);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorageService.getAccessToken();
            return axios(originalRequest);
          }
         }
       );

   }
   return Promise.reject(error);
});


export default router;
