
import Vue from 'vue';
import DashboardPlugin from './plugins/dashboard-plugin';
import App from './App.vue';
import VCalendar from 'v-calendar';
import axios from 'axios'

// router setup
import router from './routes/router';
// plugin setup
Vue.use(DashboardPlugin);
Vue.use(VCalendar, {componentPrefix:'vc',});

Vue.prototype.$baseMediaURL = 'http://localhost:8000'

// Vue.prototype.$baseMediaURL = 'http://13.213.0.73/'

window.axios = axios
// axios.defaults.baseURL = 'http://192.168.99.242:8000'
// axios.defaults.baseURL = 'http://13.213.0.73/'
axios.defaults.baseURL = 'http://localhost:8000'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router
});
